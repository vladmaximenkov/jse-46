package ru.vmaksimenkov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.dto.ISessionRecordRepository;
import ru.vmaksimenkov.tm.dto.SessionRecord;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRecordRepository extends AbstractBusinessRecordRepository<SessionRecord> implements ISessionRecordRepository {

    public SessionRecordRepository(@NotNull final EntityManager em) {
        super(em, SessionRecord.class);
    }

    @Override
    public void clear(@Nullable final String userId) {
        em.createQuery("DELETE SessionRecord where userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        em.createQuery("DELETE SessionRecord").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM SessionRecord WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM SessionRecord WHERE userId = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Nullable
    @Override
    public List<SessionRecord> findAll(@Nullable final String userId) {
        return em.createQuery("FROM SessionRecord WHERE userId = :userId", SessionRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<SessionRecord> findAll() {
        return em.createQuery("FROM SessionRecord", SessionRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
    }

    @Nullable
    @Override
    public SessionRecord findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(em.createQuery("FROM SessionRecord WHERE userId = :userId AND id = :id", SessionRecord.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return em.createQuery("SELECT COUNT(*) FROM SessionRecord WHERE userId = :userId", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public Long size() {
        return em.createQuery("SELECT COUNT(*) FROM SessionRecord", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).setMaxResults(1).getSingleResult();
    }

}
