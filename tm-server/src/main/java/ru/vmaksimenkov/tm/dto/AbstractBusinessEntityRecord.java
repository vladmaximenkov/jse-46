package ru.vmaksimenkov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractBusinessEntityRecord extends AbstractEntityRecord implements Serializable {

    @Column(name = "user_id")
    @NotNull
    private String userId;

}
