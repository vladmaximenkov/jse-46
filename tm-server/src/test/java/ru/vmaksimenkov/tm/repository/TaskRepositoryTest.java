package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.api.repository.dto.ITaskRecordRepository;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.marker.DBCategory;
import ru.vmaksimenkov.tm.repository.dto.ProjectRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.TaskRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;

import java.util.ArrayList;
import java.util.List;

public class TaskRepositoryTest extends AbstractTest {

    @NotNull
    protected static final String TEST_TASK_NAME = "TaskRecord test name";

    @NotNull
    protected static final String TEST_TASK_NAME_TWO = "Z TaskRecord test name";

    @NotNull
    private static final ITaskRecordRepository TASK_REPOSITORY = new TaskRecordRepository(ENTITY_MANAGER);

    @Nullable
    private static TaskRecord TEST_TASK;

    @NotNull
    private static String TEST_TASK_ID;

    @Test
    @Category(DBCategory.class)
    public void addAll() {
        initTwo();
        Assert.assertEquals(2, TASK_REPOSITORY.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void bindByProjectId() {
        @NotNull final String projectId = createProject();
        TASK_REPOSITORY.bindTaskPyProjectId(TEST_USER_ID, projectId, TEST_TASK_ID);
        Assert.assertNotNull(TEST_TASK);
        Assert.assertEquals(projectId, TEST_TASK.getProjectId());
    }

    @Test
    @Category(DBCategory.class)
    public void clear() {
        TASK_REPOSITORY.clear(TEST_USER_ID);
        Assert.assertTrue(TASK_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
        TASK_REPOSITORY.clear();
        Assert.assertTrue(TASK_REPOSITORY.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void create() {
        Assert.assertNotNull(TEST_TASK);
        Assert.assertNotNull(TEST_TASK.getId());
        Assert.assertNotNull(TEST_TASK.getName());
        Assert.assertEquals(TEST_TASK_NAME, TEST_TASK.getName());
        Assert.assertNotNull(TASK_REPOSITORY.findAll(TEST_USER_ID));
    }

    @NotNull
    private String createProject() {
        @NotNull final ProjectRecord project = new ProjectRecord();
        new ProjectRecordRepository(ENTITY_MANAGER).add(project);
        return project.getId();
    }

    @Test
    @Category(DBCategory.class)
    public void existsById() {
        Assert.assertTrue(TASK_REPOSITORY.existsById(TEST_USER_ID, TEST_TASK_ID));
    }

    @Test
    @Category(DBCategory.class)
    public void existsByName() {
        Assert.assertTrue(TASK_REPOSITORY.existsByName(TEST_USER_ID, TEST_TASK_NAME));
    }

    @Test
    @Category(DBCategory.class)
    public void existsByProjectId() {
        @NotNull final String projectId = createProject();
        TASK_REPOSITORY.bindTaskPyProjectId(TEST_USER_ID, projectId, TEST_TASK_ID);
        Assert.assertTrue(TASK_REPOSITORY.existsByProjectId(TEST_USER_ID, projectId));
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        Assert.assertEquals(TEST_TASK, TASK_REPOSITORY.findByIndex(TEST_USER_ID, 1));
        Assert.assertEquals(TEST_TASK_ID, TASK_REPOSITORY.getIdByIndex(TEST_USER_ID, 1));
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        Assert.assertEquals(TEST_TASK, TASK_REPOSITORY.findByName(TEST_USER_ID, TEST_TASK_NAME));
    }

    @After
    public void finishTest() {
        new UserRecordRepository(ENTITY_MANAGER).clear();
        TASK_REPOSITORY.clear();
        new ProjectRecordRepository(ENTITY_MANAGER).clear();
    }

    private void initTwo() {
        TASK_REPOSITORY.clear(TEST_USER_ID);
        @NotNull final TaskRecord task1 = new TaskRecord(TEST_TASK_NAME);
        task1.setUserId(TEST_USER_ID);
        task1.setStatus(Status.IN_PROGRESS);
        @NotNull final TaskRecord task2 = new TaskRecord(TEST_TASK_NAME_TWO);
        task2.setUserId(TEST_USER_ID);
        task2.setStatus(Status.COMPLETE);
        @NotNull final List<TaskRecord> list = new ArrayList<>();
        list.add(task1);
        list.add(task2);
        TASK_REPOSITORY.add(list);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        Assert.assertNotNull(TEST_TASK);
        TASK_REPOSITORY.remove(TEST_TASK);
        Assert.assertNull(TASK_REPOSITORY.findById(TEST_TASK.getId()));
        Assert.assertTrue(TASK_REPOSITORY.findAll(TEST_USER_ID).isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByIndex() {
        initTwo();
        TASK_REPOSITORY.removeByIndex(TEST_USER_ID, 1);
        Assert.assertEquals(TEST_TASK_NAME_TWO, TASK_REPOSITORY.findAll(TEST_USER_ID).get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByName() {
        initTwo();
        TASK_REPOSITORY.removeOneByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertEquals(TEST_TASK_NAME_TWO, TASK_REPOSITORY.findAll(TEST_USER_ID).get(0).getName());
    }

    @Before
    public void startTest() {
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final TaskRecord task = new TaskRecord();
        TEST_TASK_ID = task.getId();
        task.setName(TEST_TASK_NAME);
        task.setUserId(TEST_USER_ID);
        TASK_REPOSITORY.add(task);
        TEST_TASK = TASK_REPOSITORY.findById(TEST_USER_ID, task.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void unbind() {
        @NotNull final String projectId = createProject();
        TASK_REPOSITORY.bindTaskPyProjectId(TEST_USER_ID, projectId, TEST_TASK_ID);
        TASK_REPOSITORY.unbindTaskFromProject(TEST_USER_ID, TEST_TASK_ID);
        Assert.assertNotNull(TEST_TASK);
        Assert.assertNull(TEST_TASK.getProjectId());
    }

}
