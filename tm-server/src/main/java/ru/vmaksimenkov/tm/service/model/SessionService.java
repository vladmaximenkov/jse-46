package ru.vmaksimenkov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.model.ISessionRepository;
import ru.vmaksimenkov.tm.api.repository.model.IUserRepository;
import ru.vmaksimenkov.tm.api.service.IConnectionService;
import ru.vmaksimenkov.tm.api.service.IPropertyService;
import ru.vmaksimenkov.tm.api.service.model.ISessionService;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.user.AccessDeniedException;
import ru.vmaksimenkov.tm.model.Session;
import ru.vmaksimenkov.tm.model.User;
import ru.vmaksimenkov.tm.repository.model.SessionRepository;
import ru.vmaksimenkov.tm.repository.model.UserRepository;
import ru.vmaksimenkov.tm.util.HashUtil;
import ru.vmaksimenkov.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;
    @NotNull
    private final IPropertyService propertyService;

    public SessionService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        super(connectionService);
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @SneakyThrows
    public void add(@Nullable final List<Session> list) {
        if (list == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            repository.add(list);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public void add(@Nullable final Session session) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            repository.add(session);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login) || isEmpty(password)) return false;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) return false;
            if (user.isLocked()) return false;
            @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
            if (isEmpty(passwordHash)) return false;
            return passwordHash.equals(user.getPasswordHash());
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            repository.clear(userId);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            repository.clear();
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void close(@NotNull final Session session) {
        validate(session);
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            repository.remove(session);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void closeAll(@NotNull final Session session) {
        validate(session);
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            repository.clear(session.getUser().getId());
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            return repository.existsById(id);
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            return repository.existsById(userId, id);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Session> findAll(@Nullable final String userId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            return repository.findAll(userId);
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            return repository.findAll();
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            return repository.findById(userId, id);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findById(@NotNull final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            return repository.findById(id);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public Session findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            return repository.findByIndex(userId, index);
        } finally {
            em.close();
        }
    }

    @Override
    public @Nullable String getIdByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            return repository.getIdByIndex(userId, index);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Session> getListSession(@NotNull final Session session) {
        validate(session);
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            return repository.findAll(session.getUser().getId());
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User getUser(@NotNull final Session session) {
        @NotNull final String userId = getUserId(session);
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            return repository.findById(userId);
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final Session session) {
        validate(session);
        return session.getUser().getId();
    }

    @Override
    public boolean isValid(@NotNull final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check || isEmpty(login)) return null;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) return null;
            @NotNull final Session session = new Session();
            session.setUser(user);
            session.setTimestamp(System.currentTimeMillis());
            add(session);
            em.getTransaction().commit();
            return sign(session);
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Session session) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRepository sessionRepository = new SessionRepository(em);
            sessionRepository.remove(session);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            repository.removeById(id);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRepository sessionRepository = new SessionRepository(em);
            sessionRepository.removeById(userId, id);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            repository.removeByIndex(userId, index);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String salt = propertyService.getSessionSalt();
        final int cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        update(session);
        return session;
    }

    @Override
    @SneakyThrows
    public Long size() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            return repository.size();
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public Long size(@Nullable final String userId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(em);
            return repository.size(userId);
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public void update(@Nullable final Session session) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRepository sessionRepository = new SessionRepository(em);
            sessionRepository.update(session);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUser().getId();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(em);
            @Nullable final User user = repository.findById(userId);
            if (user == null) throw new AccessDeniedException();
            if (user.getRole() == null) throw new AccessDeniedException();
            if (!role.equals(user.getRole())) throw new AccessDeniedException();
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUser().getId())) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionSign = sign(temp);
        if (sessionSign == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository sessionRepository = new SessionRepository(em);
            if (!sessionRepository.existsById(session.getId())) throw new AccessDeniedException();
        } finally {
            em.close();
        }
    }
}
