package ru.vmaksimenkov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserRecord extends AbstractEntityRecord {

    @Column
    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Column
    private boolean locked = false;

    @Column
    @Nullable
    private String login;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Nullable
    @Column(name = "password_hash")
    private String passwordHash;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

}
