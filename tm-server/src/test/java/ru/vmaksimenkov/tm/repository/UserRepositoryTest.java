package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.api.repository.dto.IUserRecordRepository;
import ru.vmaksimenkov.tm.marker.DBCategory;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;

public class UserRepositoryTest extends AbstractTest {

    @NotNull
    private static final String TEST_PASSWORD = "test-password";
    @NotNull
    private static final IUserRecordRepository USER_REPOSITORY = new UserRecordRepository(ENTITY_MANAGER);

    @Test
    public void existByEmail() {
        Assert.assertTrue(USER_REPOSITORY.existsByEmail(TEST_USER_EMAIL));
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(TEST_USER, USER_REPOSITORY.findByLogin(TEST_USER_NAME));
    }

    @After
    public void finishTest() {
        USER_REPOSITORY.clear();
    }

    @Test
    @Category(DBCategory.class)
    public void removeByLogin() {
        USER_REPOSITORY.removeByLogin(TEST_USER_NAME);
        Assert.assertTrue(USER_REPOSITORY.findAll().isEmpty());
    }

    @Before
    public void startTest() {
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
    }

}
