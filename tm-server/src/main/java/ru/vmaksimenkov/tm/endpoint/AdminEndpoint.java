package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.endpoint.IAdminEndpoint;
import ru.vmaksimenkov.tm.api.service.ServiceLocator;
import ru.vmaksimenkov.tm.component.Backup;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    private Backup backup;

    public AdminEndpoint() {
        super(null);
    }

    public AdminEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final Backup backup
    ) {
        super(serviceLocator);
        this.backup = backup;
    }

    @Override
    @WebMethod
    public void loadBackup(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.load();
    }

    @Override
    @WebMethod
    public void loadJson(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.loadJson();
    }

    @Override
    @WebMethod
    public void saveBackup(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.run();
    }

    @Override
    @WebMethod
    public void saveJson(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.saveJson();
    }

}
